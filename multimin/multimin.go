/*
 *   Copyright (C) 2012 Philip Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */
package multimin

/*
#cgo pkg-config: gsl
#include "multimin.h"
#include <gsl/gsl_vector.h>
*/
import "C"
import "unsafe"
import "fmt"

type F interface {
	Evaluate([]float64) float64
	DEvaluate([]float64) []float64
	N() int
}

type T int

const (
	GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_FR = iota
	GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_PR
	GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS2
	GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS
	GSL_MULTIMIN_FDFMINIMIZER_STEEPEST_DESCENT
)

func (t T) gslType() (gt *C.gsl_multimin_fdfminimizer_type, err error) {
	switch t {
	case GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_FR:
		return C.gsl_multimin_fdfminimizer_conjugate_fr, nil
	case GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_PR:
		return C.gsl_multimin_fdfminimizer_conjugate_pr, nil
	case GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS2:
		return C.gsl_multimin_fdfminimizer_vector_bfgs2, nil
	case GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS:
		return C.gsl_multimin_fdfminimizer_vector_bfgs, nil
	case GSL_MULTIMIN_FDFMINIMIZER_STEEPEST_DESCENT:
		return C.gsl_multimin_fdfminimizer_steepest_descent, nil
	}
	return nil, fmt.Errorf("unknown type: %v", t)
}

func (t T) String() string {
	switch t {
	case GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_FR:
		return "Fletcher-Reeves conjugate gradient algorithm"
	case GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_PR:
		return "Polak-Ribiere conjugate gradient algorithm"
	case GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS2:
		return "Broyden-Fletcher-Goldfarb-Shanno (BFGS) algorithm 2"
	case GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS:
		return "Broyden-Fletcher-Goldfarb-Shanno (BFGS) algorithm"
	case GSL_MULTIMIN_FDFMINIMIZER_STEEPEST_DESCENT:
		return "Steepest descent algorithm"
	}
	return "unknown"
}

type ftype struct {
	function F
}

func (f *ftype) evaluate(x []float64) float64 {
	return f.function.Evaluate(x)
}

func (f *ftype) d_evaluate(x []float64) []float64 {
	return f.function.DEvaluate(x)
}

//exported function for C module to deal with Go F Object
//export fevaluate
func fevaluate(x *C.gsl_vector, p unsafe.Pointer) C.double {
	f := (*ftype)(p)
	v := vectorToSlice(x)
	r := C.double(f.evaluate(v))
	return r
}

//export dfevaluate
func dfevaluate(x *C.gsl_vector, p unsafe.Pointer, df *C.gsl_vector) {
	f := (*ftype)(p)
	v := vectorToSlice(x)
	for i, x := range f.d_evaluate(v) {
		C.gsl_vector_set(df, C.size_t(i), C.double(x))
	}
}

//export fdfevaluate
func fdfevaluate(x *C.gsl_vector, p unsafe.Pointer, f *C.double, df *C.gsl_vector) {
	*f = fevaluate(x, p)
	dfevaluate(x, p, df)
}

type FDFMinimizer struct {
	f F
	x *C.gsl_vector
	t *C.gsl_multimin_fdfminimizer_type
	s *C.gsl_multimin_fdfminimizer
}

// NewFDFminimizer creates a minimizer s to minimize the function fdf starting
// from the initial point x. The size of the first trial step is given by
// stepSize. The accuracy of the line minimization is specified by tol. The
// precise meaning of this parameter depends on the method used. Typically the
// line minimization is considered successful if the gradient of the function g
// is orthogonal to the current search direction p to a relative accuracy of
// tol, where dot(p,g) < tol |p| |g|.
func NewFDFMinimizer(t T, f F, x []float64, stepSize, tol float64) (m *FDFMinimizer, err error) {
	m = &FDFMinimizer{
		f: f,
	}
	if m.t, err = t.gslType(); err != nil {
		return
	}
	m.x = C.gsl_vector_alloc(C.size_t(f.N()))
	m.s = C.gsl_multimin_fdfminimizer_alloc(m.t, C.size_t(f.N()))
	for i := 0; i < f.N(); i++ {
		C.gsl_vector_set(m.x, C.size_t(i), C.double(x[i]))
	}
	var my_func C.gsl_multimin_function_fdf
	my_func = C.wrapper_function(C.int(f.N()), unsafe.Pointer(&f))
	C.gsl_multimin_fdfminimizer_set(m.s, &my_func, m.x, C.double(stepSize), C.double(tol))
	return
}

// Iterate performs a single iteration of the minimizer s. If the iteration
// encounters an unexpected problem then an error code will be returned.
func (m *FDFMinimizer) Iterate() (err error) {
	status := C.gsl_multimin_fdfminimizer_iterate(m.s)
	if status != C.GSL_SUCCESS {
		return fmt.Errorf("gsl_multimin_fdfminimizer_iterate status: %v", status)
	}
	return nil
}

// TestGradient tests the norm of the gradient g against the absolute tolerance
// epsabs. The gradient of a multidimensional function goes to zero at a
// minimum. The test returns done = true if the following condition is achieved,
// |g| < epsabs and returns done = false otherwise. A suitable choice of epsabs
// can be made from the desired accuracy in the function for small variations
// in x. The relationship between these quantities is given by
// \delta f = g \delta x.
func (m *FDFMinimizer) TestGradient(epsabs float64) (done bool, err error) {
	status := C.gsl_multimin_test_gradient(m.s.gradient, C.double(epsabs))
	if status == C.GSL_SUCCESS {
		return true, nil
	} else if status == C.GSL_CONTINUE {
		return false, nil
	}
	return false, fmt.Errorf("gsl_multimin_test_gradient status is not GSL_CONTINUE: %v", C.gsl_strerror(status))
}

// X is the current search position.
func (m *FDFMinimizer) X() []float64 {
	return vectorToSlice(m.s.x)
}

// FuncValue is the function value at the search position.
func (m *FDFMinimizer) FuncValue() float64 {
	return float64(m.s.f)
}

// Free frees all allocated resources, rendering the Minimizer unusuable for
// further operations.
func (m *FDFMinimizer) Free() {
	if m.x != nil {
		C.gsl_vector_free(m.x)
		m.x = nil
	}
	if m.s != nil {
		C.gsl_multimin_fdfminimizer_free(m.s)
		m.s = nil
	}
}

func vectorToSlice(x *C.gsl_vector) (v []float64) {
	v = make([]float64, x.size)
	for i := 0; i < len(v); i++ {
		v[i] = float64(C.gsl_vector_get(x, C.size_t(i)))
	}
	return
}
